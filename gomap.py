#!/usr/bin/env python2

'''
Importing all the modules necessary for running the pipeline
pprint is only needed for debugging purposes
'''
import  os, re, logging, json, sys, argparse, jsonmerge, yaml
from argparse import RawTextHelpFormatter
from pprint import pprint
from code.gomap_preprocess import run_preprocess
from code.gomap_aggregate import aggregate
from code.gomap_setup import setup
from code.utils.basic_utils import init_dirs, copy_input
from code.utils.logging_utils import setlogging

from jsonmerge import Merger
schema = {
             "properties": {
                 "bar": {
                     "mergeStrategy": "append"
                 }
             }
         }
merger = Merger(schema)

'''
    Parsing the input config file that will be supplied by te user.
'''
main_parser = argparse.ArgumentParser(description='Welcome to running the GO-MAP pipeline',formatter_class=RawTextHelpFormatter)
main_parser.add_argument('--config',help="The config file in yml format. \nPlease see config.json for an example. \nIf a config file is not provided then the default parameters will be used.",required=True)
main_parser.add_argument('--step',help="GO-MAP has two distinct steps. Choose the step to run \n1) preprocess \n2) annotate", choices=['preprocess','aggregate','init','setup'],required=True)
main_args = main_parser.parse_args()

'''
    This section loads the pipeline base config file from the pipeline script
    location. and loads the pipleine configutation
'''

with open("config/pipeline.yml") as tmp_file:
    pipe_config = yaml.load(tmp_file)

config_file = pipe_config["input"]["workdir"]+"/"+main_args.config
with open(config_file) as tmp_file:
    user_config = yaml.load(tmp_file)
    user_config["input"]["workdir"] = os.path.dirname(config_file)

config = merger.merge(pipe_config, user_config)
config = init_dirs(config)

setlogging(config)

conf_out = config["input"]["gomap_dir"]+"/"+config["input"]["basename"]+".all.yml"
config["input"]["config_file"] = conf_out
with open(conf_out,"w") as out_f:
	yaml.dump(config,out_f)


# logging_config = config["logging"]
# log_file = config["input"]["gomap_dir"] + "/log/" + config["input"]["basename"] + '.log'
# gomap_logger.basicConfig(filename=log_file,level=logging_config['level'],filemode='w+',format=logging_config["format"],datefmt=logging_config["formatTime"])
# logger = gomap_logger.getLogger("gomap")
# logger.info("Starting to run the pipline for " + config["input"]["basename"])


'''
Depending the step selected by the user we are going to run the relevant part of GO-MAP
'''

if main_args.step == "preprocess":
    logging.info("Running Pre-processing Step")
    copy_input(config)
    run_preprocess(config)
elif main_args.step == "aggregate":
    logging.info("Running Aggregate Step")
    aggregate(config)
elif main_args.step == "setup":
    logging.info("Downloading data from CyVerse")
    setup(config)

