.. GO-MAP documentation master file, created by
   sphinx-quickstart on Thu Dec 21 10:52:19 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GO-MAP's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   INTRO
   REQUIRE
   SETUP
   STEPS
   source/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
